# web component example
A vanilla js approach to web components.

Following components can be found: 

1. die
1. counter

Just open my-page.html in a local server, or from your IDE.

Please find the example here: [gitlab pages](https://jf-lab.gitlab.io/webcomponents/my-page.html)