/*
    This file includes my counter tag
 */

class Counter extends HTMLElement {
  constructor() {
    super();
  }
  addClickEventListener(myElem) {
    myElem.addEventListener('mouseover', () => {
      this.number++;
      myElem.innerHTML = this.number;
    })
  }
  connectedCallback() {
    this.attachShadow({mode: 'open'});
    this.number = 0;

    const div = document.createElement('div');
    div.innerHTML = this.number;
    this.addClickEventListener(div);
    this.shadowRoot.appendChild(div);
  }
}

customElements.define('my-counter', Counter)
