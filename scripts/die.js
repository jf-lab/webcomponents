class Die extends HTMLElement {
  constructor() {
    super();
  }

  connectedCallback() {
    this.attachShadow({mode:'open'});
    this.number = this.getRandomArbitrary(1, 7);
    const div = document.createElement('div');
    div.innerHTML = this.number;
    this.shadowRoot.appendChild(div);
  }
  getRandomArbitrary(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min) + min);
  }
}

customElements.define('my-die', Die)
